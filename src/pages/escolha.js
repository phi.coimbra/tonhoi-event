import React, { useState, useContext, useEffect } from "react";
import _ from "lodash";
import { useSpring, animated } from "react-spring";
import useWindowDimensions, {
  Dispatch,
  aviso,
  currencyBack,
  postServerGeraPix,
  sendCelularMessage,
} from "../hooks/hooks";
import {
  Button,
  Popup,
  Container,
  Divider,
  Header,
  Icon,
  Dropdown,
  Label,
  Modal,
  ModalActions,
  ModalContent,
  ModalDescription,
  ModalHeader,
  Grid,
  GridColumn,
  Input,
} from "semantic-ui-react";

export const Escolha = (props) => {
  const [open, setOpen] = useState(false);
  const [periodo, setPeriodo] = useState("All Day");
  const [loading, setLoading] = useState(false);
  const [code, setCode] = useState("");
  const [codeExist, setCodeExist] = useState(false);
  const { mobile } = useWindowDimensions();

  const [play, toggle] = useState(true);

  const reducer = useContext(Dispatch);
  const [{ userdata, ingressos, discount_codes }] = reducer;

  const [cart, setCart] = useState({ adulto: 0, infantil: 0 });

  const [options, setOptions] = useState([]);

  const { x, y } = useSpring({
    from: { y: play ? -window.innerHeight : 0, opacity: play ? 0 : 1 },
    to: { y: play ? 0 : -window.innerHeight, opacity: play ? 1 : 0 },
    onRest: () => !play && props.history.push("/pagamento"),
  });

  useEffect(() => {
    console.log(ingressos);
  }, [ingressos]);

  useEffect(() => {
    const haveDiscount = discount_codes.filter((a) => {
      console.log(a.code, code);
      return a.code === code;
    });

    if (haveDiscount.length > 0) {
      setCodeExist(haveDiscount[0].value);
    } else {
      setCodeExist(false);
    }
  }, [code]);

  useEffect(() => {
    updateCounter();
  }, [cart]);

  const updateCounter = () => {
    let tickets = [];
    for (let [index, [key, value]] of Object.entries(Object.entries(cart))) {
      if (value !== 0) {
        tickets.push({
          key: Number(index) + 1,
          text: key,
          value: key,
          count: value,
        });
      }

      console.log(tickets);
    }
    //console.log(tickets);
    setOptions(tickets);
  };

  const updateDiscountCode = (_e, a) => {
    setCode(a.value.toUpperCase());
  };

  const getTicketQuantity = () => {
    let total = 0;
    for (let [_index, [_key, value]] of Object.entries(Object.entries(cart))) {
      total += value;
    }

    return total;
  };

  const getTicketTotalValue = (type) => {
    let total = 0;
    for (let [_index, [key, value]] of Object.entries(Object.entries(cart))) {
      total += Number(value) * Number(getPropsByValues(key, type));
    }

    return new Intl.NumberFormat("pt-BR").format(total);
  };

  const renderLabel = (i, type) => ({
    color: "blue",
    content: `${i.count} ${i.count > 1 ? "Ingressos" : "Ingresso"} - ${i.text}`,
  });

  const handleChange = (e, item) =>
    console.log(
      item,
      e,
      Object.keys(options).map((key) => options[key].value)
    );

  const addTicket = (type) => {
    setCart((prevValue) => {
      return {
        ...prevValue,
        [type]: prevValue[type] + 1,
      };
    });
  };

  const removeTicket = (_e, type) => {
    setCart((prevValue) => {
      return {
        ...prevValue,
        [type.value]: prevValue[type.value] - 1,
      };
    });
  };

  const geraCompra = async (type, value) => {
    setLoading(true);
    //console.log(type);
    try {
      const response = await postServerGeraPix({
        customer: userdata.customer,
        period: "All Day",
        value: currencyBack(value.toString()).toString(),
        billingType: type,
        description: getDescriptionInfo(),
        quantity: cart,
        validation_token: "klkasjMOSmOAMsFsm",
      });
      console.log(response);

      await sendCelularMessage(
        "55" + userdata.mobilePhone,
        userdata.mobilePhone,
        `Agradecemos seu interesse em participar deste evento memorável de Ano-Novo Chinês 2025 - Ano da Serpente - no Mirante Congonhas.\nFalta muito pouco: segue abaixo seu link para efetivar a compra do(s) convite(s) selecionados anteriormente.\n\n(Caso não esteja visualizando o link, por favor, salve este contato que automaticamente o link ficará disponível)\nAbaixo, está o seu link de pagamento\n${response.data.invoiceUrl}\n\n(Sim, o pagamento será através de nosso parceiro Asaas)`
      );

      toggle(!play);
    } catch (err) {
      aviso("Erro em gerar o link de cobrança, tente novamente", "error");
    }
    setLoading(false);
  };

  const getDescriptionInfo = () => {
    if (cart.adulto > 0 && cart.infantil > 0) {
      return `Compra de ingresso ${cart.adulto} adulto(s) e ${cart.infantil} infantil`;
    } else if (cart.adulto > 0 && cart.infantil == 0) {
      return `Compra de ingresso ${cart.adulto} adulto(s)`;
    } else {
      return `Compra de ingresso ${cart.infantil} infantil`;
    }
  };

  const escolhePeriodo = (e, a) => {
    console.log(a);
    setOpen(true);
    setPeriodo("All Day");
  };

  const getPropsByValues = (category, type) => {
    console.log(category, type);
    const results = ingressos.filter((element) => {
      return element.category === category && element.billingType === type;
    });
    console.log(codeExist);
    return codeExist || results[0]?.value;
  };

  const openModal = () => {
    setCart({ adulto: 0, infantil: 0 });
    setOpen(false);
  };

  return (
    <div className="frame">
      <div className="central">
        <animated.div
          style={Object.assign({ x, y, width: mobile ? "100%" : 500 })}
        >
          <div className="pages">
            <Container fluid>
              <Header as="h2">Em primeira mão:</Header>
              <Divider />
              <div style={{ marginTop: 5, fontSize: 16 }}>
                <p>
                  Além da comemoração do Ano-Novo chinês, esse é o evento
                  oficial para o lançamento da nova carta de drinks do novo bar
                  do Ton Hoi. Poderão ser provados, à vontade, todos os drinks,
                  disponíveis para o evento, além dos mocktails. Serviremos
                  também vinhos branco e rosé de nossos parceiros.
                </p>
                <p>
                  O serviço de comidinhas neste evento será totalmente volante
                  (os garçons circularão por todo salão - não haverão mesas) e,
                  assim como os drinks, alguns petiscos que também estarão em
                  nosso novo bar. E sim, teremos algumas opções vegetarianas.
                  Para sobremesa, também teremos novidades com novos parceiros!
                </p>
              </div>
            </Container>
            <div
              style={{
                fontSize: 20,
                lineHeight: 1,
                color: "#000000",
              }}
            ></div>

            <Button
              style={{ marginTop: 10 }}
              onClick={escolhePeriodo}
              fluid
              secondary
              size="big"
            >
              {"Comprar"}
            </Button>
          </div>
        </animated.div>

        <Modal
          style={{ maxWidth: 580 }}
          closeIcon
          onClose={() => openModal(false)}
          open={open}
          trigger={<div></div>}
        >
          <ModalHeader>{`Ingresso`}</ModalHeader>
          <ModalContent>
            <ModalDescription>
              <Header style={{ marginTop: -10 }}>Sobre o convite</Header>
              <Container textAlign="justified">
                <p>
                  Este convite garante sua entrada para celebrar o Ano-Novo
                  Chinês conosco, repleto de atrações de dança, música, show, e
                  muita novidade de drinks, comidinhas e sobremesa. Tudo à
                  vontade para aproveitar como preferir.
                </p>
                <p>
                  Aperte o botão abaixo para adicionar a quantidade de ingressos
                  correspondentes ao carrinho.
                </p>
              </Container>
            </ModalDescription>
          </ModalContent>
          <ModalActions style={{ textAlign: "center" }}>
            <div style={{ marginBottom: 10 }}>
              <Input
                label={{
                  icon: "asterisk",
                  color: codeExist ? "green" : "grey",
                }}
                labelPosition="right corner"
                color="green"
                name="discount"
                focus
                placeholder="Código promocional"
                onChange={updateDiscountCode}
                value={code}
              />
            </div>
            <Button as="div" labelPosition="right">
              <Button
                color="black"
                // onClick={() => geraCompra(getPropsByValues(periodo, "PIX"))}
                onClick={() => addTicket("adulto")}
                style={{
                  width: 170,
                  lineHeight: 1.7,
                  justifyContent: "left",
                }}
              >
                <Icon name="shop" />
                {loading ? "Aguarde" : "Ingresso Adulto"}
              </Button>

              <Label as="a" basic color="green" pointing="left">
                <div
                  style={{
                    fontSize: 11,
                    display: "flex",
                    justifyContent: "flex-end",
                    flexDirection: "column",
                  }}
                >
                  a partir de:{" "}
                  <div style={{ fontSize: 14, marginTop: 7, color: "green" }}>
                    <Icon name="tag" />{" "}
                    {`R$ ${new Intl.NumberFormat("pt-BR").format(
                      getPropsByValues("adulto", "PIX")
                    )}`}
                  </div>
                </div>
              </Label>
            </Button>

            {/*<Button as="div" labelPosition="right">
              <Button
                fluid
                color="green"
                onClick={() => addTicket("infantil")}
                style={{ width: 170, lineHeight: 1.7 }}
              >
                <Icon name="shop" />
                {loading ? "Aguarde" : "Ingresso Infantil"}
              </Button>
              <Label as="a" basic color="green" pointing="left">
                {`R$ ${new Intl.NumberFormat("pt-BR").format(
                  getPropsByValues("infantil", "PIX")
                )}`}
              </Label>
            </Button>*/}
          </ModalActions>

          <div style={{ textAlign: "center" }}>
            <Dropdown
              multiple
              style={{ marginRight: -50 }}
              icon=""
              options={options}
              onChange={handleChange}
              onLabelClick={(e, item) => removeTicket(e, item)}
              value={Object.keys(options).map((key) => options[key].value)}
              placeholder="Escolha um ingresso acima."
              renderLabel={renderLabel}
            />
          </div>
          <div
            style={{
              textAlign: "center",
              marginTop: 10,
              marginBottom: 10,
            }}
          >
            <Popup
              wide
              trigger={
                <Button
                  content="Comprar"
                  icon="shop"
                  disabled={getTicketQuantity() < 1}
                  color="green"
                />
              }
              on="click"
              position="top center"
            >
              <p>
                {`Você está comprando ${getTicketQuantity()} ingresso(s). Selecione a forma de pagamento para receber o link de pagamento.`}
              </p>
              <Grid divided columns="equal">
                <GridColumn>
                  <Button
                    disabled={loading}
                    loading={loading}
                    color="green"
                    content={`Crédito  R$${getTicketTotalValue("CREDIT_CARD")}`}
                    fluid
                    size="small"
                    onClick={() =>
                      geraCompra(
                        "CREDIT_CARD",
                        getTicketTotalValue("CREDIT_CARD")
                      )
                    }
                  />
                </GridColumn>
                <GridColumn>
                  <Button
                    disabled={loading}
                    loading={loading}
                    color="green"
                    content={`Pix  R$${getTicketTotalValue("PIX")}`}
                    fluid
                    size="small"
                    onClick={() =>
                      geraCompra("PIX", getTicketTotalValue("PIX"))
                    }
                  />
                </GridColumn>
              </Grid>
            </Popup>
          </div>
        </Modal>
      </div>
    </div>
  );
};
