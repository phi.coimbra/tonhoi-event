import React, { useContext, useEffect, useState } from "react";
import useWindowDimensions, { Dispatch } from "../hooks/hooks";
import { useParams } from "react-router-dom";
import {
  Button,
  Container,
  Divider,
  Header,
  Icon,
  Label,
  Menu,
  MenuItem,
  Message,
} from "semantic-ui-react";
import { Validador } from "../components/validador";

export const Home = (props) => {
  const [open, _setOpen] = useState(false);
  const { mobile } = useWindowDimensions();
  const reducer = useContext(Dispatch);
  const [{ validado }, dispatch] = reducer;

  const params = useParams();

  useEffect(() => {
    console.log(params);
    if (params?.qr) {
      props.history.push("/ingresso", { params });
    }
  }, [params]);

  useEffect(() => {
    if (validado) {
      props.history.push("/form");
    }
  }, [validado]);

  const cadastrar = () => {
    const valid = localStorage.getItem("validado");
    if (valid) {
      dispatch({
        type: "validado",
        payload: true,
      });
    } else {
      dispatch({
        type: "modal",
        payload: true,
      });
    }
  };

  return (
    <div className="frame">
      <Validador open={open} />
      <div className="central">
        <div className="pages" style={{ width: mobile ? "100%" : 500 }}>
          <Container fluid>
            <Container textAlign="justified">
              <Header as="h2">Sobre o evento</Header>
              <Divider />
              <div style={{ marginBottom: 12, fontSize: 12 }}>
                <Message warning>
                  Lote vigente pode variar de acordo com a proximidade do evento
                  ou a disponibilidade de convites para venda, o que ocorrer
                  primeiro.
                </Message>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-evenly",
                  paddingTop: 10,
                }}
              >
                <Menu compact>
                  <MenuItem as="a" disabled>
                    <div
                      style={{
                        fontSize: 11,
                        display: "flex",
                        justifyContent: "flex-end",
                        flexDirection: "column",
                      }}
                    >
                      a partir de:{" "}
                      <div style={{ fontSize: 14, marginTop: 7 }}>
                        <Icon name="tag" /> R$ 467
                      </div>
                    </div>
                    <Label color="grey" style={{ width: 53 }} floating>
                      Lote 1
                    </Label>
                  </MenuItem>
                </Menu>
                <Menu compact>
                  <MenuItem as="a" disabled>
                    <div
                      style={{
                        fontSize: 11,
                        display: "flex",
                        justifyContent: "flex-end",
                        flexDirection: "column",
                      }}
                    >
                      a partir de:{" "}
                      <div style={{ fontSize: 14, marginTop: 7 }}>
                        <Icon name="tag" /> R$ 530
                      </div>
                    </div>
                    <Label color="grey" style={{ width: 53 }} floating>
                      Lote 2
                    </Label>
                  </MenuItem>
                </Menu>
                <Menu compact>
                  <MenuItem as="a">
                    <div
                      style={{
                        fontSize: 11,
                        display: "flex",
                        justifyContent: "flex-end",
                        flexDirection: "column",
                      }}
                    >
                      a partir de:{" "}
                      <div style={{ fontSize: 14, marginTop: 7 }}>
                        <Icon name="tag" /> R$ 638
                      </div>
                    </div>
                    <Label color="green" style={{ width: 53 }} floating>
                      Lote 3
                    </Label>
                  </MenuItem>
                </Menu>
              </div>
              <Divider />
              <p>
                Há mais de 4 milênios os chineses contam os anos através do
                calendário lunar, cujo Ano-Novo será regido pelo signo da
                Serpente, comemorando a entrada do ano 4723.
              </p>
              <p>
                Celebraremos com uma superfesta no Rooftop do Mirante Congonhas,
                com uma vista deslumbrante, no dia 28/01/2025, das 18h à
                meia-noite. Teremos muitas atrações tradicionais da cultura
                chinesa e muito mais. Não se preocupe, um serviço volante de
                comida, além de 2 bares estarão à disposição - para provarem
                algumas de nossas novas surpresas para o menu de drinks, que
                será apresentado em primeira mão no evento (e, tudo isso à
                vontade!).
              </p>
              <p>
                <b>
                  Garanta agora mesmo seu convite e venha conosco participar
                  deste momento inesquecível!
                </b>
              </p>
            </Container>
          </Container>
          <Button
            color="green"
            size="big"
            fluid
            as="div"
            labelPosition="right"
            style={{ marginTop: 20, marginBottom: 20 }}
          >
            <Button icon positive fluid size="big" onClick={cadastrar}>
              Fazer a reserva
            </Button>
            <Label
              as="a"
              basic
              pointing="left"
              color="green"
              onClick={() =>
                (window.location.href = "http://evento.tonhoi.me/faq")
              }
            >
              <Icon name="help" style={{ margin: 0 }} />
            </Label>
          </Button>
        </div>
      </div>
    </div>
  );
};
