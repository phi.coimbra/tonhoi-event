import React, { useEffect, useState } from "react";
import _ from "lodash";
import { useLocation } from "react-router-dom/cjs/react-router-dom";
import {
  Button,
  ButtonContent,
  Header,
  HeaderContent,
  HeaderSubheader,
  Icon,
  Input,
  Label,
  Message,
  Table,
  TableBody,
  TableCell,
  TableRow,
} from "semantic-ui-react";
import { aviso, getGuestTicket, saveGuestTicket } from "../hooks/hooks";

const url = window.location.origin;

export const Guest = () => {
  const [tickets, setTickets] = useState([]);
  const [loading, setLoading] = useState(false);

  const location = useLocation();

  useEffect(() => {
    if (location) {
      console.log(location);
      const userId = location?.pathname?.split("/");
      getFields(userId[2]);
    }
  }, [location]);

  const getFields = async (user) => {
    try {
      const response = await getGuestTicket({ user });
      console.log(response);
      setTickets(response.data);
    } catch (e) {
      console.log("ERRO");
    }
  };

  const editField = async (input, i) => {
    let cgField = [...tickets];
    cgField[i].guest = input.value;
    setTickets(cgField);
  };

  const saveFields = async (data) => {
    setLoading(true);
    const save = await saveGuestTicket(data);
    if (save) {
      aviso("Convidado salvo", "success");
    } else {
      aviso("Erro ao salvar o convidado", "error");
    }
    setLoading(false);
  };

  const buildFields = () => {
    if (tickets.length > 0) {
      return tickets.map((field, i) => (
        <TableRow key={i + 1}>
          <TableCell width={1} style={{ textAlign: "center" }}>
            <Label circular color={"blue"}>
              {i + 1}
            </Label>
          </TableCell>
          <TableCell>
            <Input
              name="name"
              fluid
              focus
              placeholder={field.name}
              onChange={(_e, input) => editField(input, i)}
              value={field.guest}
            />
          </TableCell>
          <TableCell width={2}>
            {
              <Button
                loading={loading}
                color={field.guest === "" ? "grey" : "green"}
                disabled={field.guest === ""}
                onClick={() =>
                  saveFields({
                    id: tickets[i].id,
                    guest: tickets[i].guest,
                  })
                }
              >
                Salvar
              </Button>
            }
          </TableCell>
        </TableRow>
      ));
    } else {
      return <TableRow></TableRow>;
    }
  };

  return (
    <div className="frame">
      <div style={{ maxWidth: 600, margin: "auto", padding: 30 }}>
        <Header as="h2" dividing>
          <Icon name="user" />
          <HeaderContent>
            Edição de convidados
            <HeaderSubheader>
              Coloque o nome das pessoas que utilizarão os convites
            </HeaderSubheader>
          </HeaderContent>
        </Header>
        <Table definition>
          <TableBody>{buildFields()}</TableBody>
        </Table>
        <Input
          readOnly
          fluid
          label={
            <Button
              animated="vertical"
              color="green"
              onClick={() =>
                navigator.clipboard.writeText(tickets?.[0]?.ticketShortUrl)
              }
            >
              <ButtonContent visible>Copiar</ButtonContent>
              <ButtonContent hidden>Copiado</ButtonContent>
            </Button>
          }
          labelPosition="right"
          defaultValue={tickets?.[0]?.ticketShortUrl}
        />
        <Message attached="bottom" success>
          <Icon name="info" />O link do convite pode ser usado por todos os
          convidados. É o mesmo link enviado pelo whatsapp.
        </Message>
      </div>
    </div>
  );
};
