import React, { useState } from "react";
import {
  Accordion,
  AccordionContent,
  AccordionTitle,
  Container,
  Divider,
  Header,
  Icon,
} from "semantic-ui-react";
import { Header as Top } from "../pages/header";
import useWindowDimensions from "../hooks/hooks";

export const Faq = () => {
  const [activeIndex, setActiveIndex] = useState(0);
  const faqText = [
    {
      title: "O que é a festa?",
      description:
        "A festa é uma comemoração do Ano-Novo Chinês que, diferentemente do ocidental, que conta o ano através do calendário gregoriano, os chineses contam pelo calendário lunar, o que faz que esta data não seja a mesma todos os anos. Além disso, cada ano é representado por 1 dos 12 animais determinados no calendário e 2025 será o ano da serpente.",
    },
    {
      title: "Quando será",
      description:
        "A festa será no dia 28/01/2025 na terça-feira, das 18h00 à meia-noite. Este novo ano se iniciará no dia 29/01/2025 (o que corresponde ao dia 01/01 dos ocidentais).",
    },
    {
      title: "Onde será a festa e qual a melhor forma de acesso?",
      description:
        "A festa será no Mirante Congonhas, localizado na Avenida Washington Luís, 6675 - 11º Andar (Rooftop) - São Paulo. Recomendamos que a entrada seja feita pelo estacionamento na Rua Renascença, 103 (rua lateral ao prédio), pois não há recuo para carros na movimentada avenida. Para evitar acidentes, sugerimos que todos, inclusive quem vier de motorista de aplicativo, utilizem o estacionamento.",
    },
    {
      title: "O que vai ter nessa festa?",
      description:
        "É uma festa temática cultural chinesa em comemoração ao Ano-Novo. Teremos algumas atrações bem típicas como Dança do Leão, danças chinesas, Ópera de Sichuan, além do lançamento oficial de bebidas selecionadas da nova carta de drinks do novo bar do Ton Hoi (sim, quem estiver no evento poderá provar em primeira mão antes de todo mundo), além de comidinhas que tampouco foram vistas no menu do Ton Hoi, que circularão volantemente pelos garçons (exato, não teremos mesas como de costume no restaurante, pois trata-se de uma festa). ",
    },
    {
      title:
        "Quais serão as opções de comida e bebida? Sou vegetariano e não bebo.",
      description:
        "Teremos diversas opções de comidas e bebidas, muitas delas especialmente selecionadas para este evento. Pensamos em tudo com muito carinho, incluindo opções vegetarianas e drinks sem álcool (mocktails), além de água e refrigerantes.",
    },
    {
      title: "Como posso adquirir ingressos e o que está incluído?",
      description: `Os ingressos são limitados para garantir o conforto e a segurança de todos os convidados e estão disponíveis para compra no site https://evento.tonhoi.me. O valor é único e inclui acesso livre às comidas e bebidas servidas durante o evento.`,
    },
    {
      title: "Crianças podem participar do evento? Há desconto?",
      description:
        "Sim, crianças podem participar do evento temático. Recomendamos a participação de crianças a partir de 6 anos, pois o evento é longo e pode ser cansativo para os mais pequenos. Queremos que todos aproveitem a noite especial sem aborrecimentos. Concedemos desconto para crianças até 12 anos completos. Siga o passo-a-passo normal de compra para um adulto e, em seguida, entre em contato conosco pelo WhatsApp (11 99100 8777) informando a compra para criança e aguarde as instruções.",
    },
    {
      title: "O local conta com uma área kids?",
      description:
        "Infelizmente, não. Reforçamos que é um evento longo e, por isso, sugerimos a participação de crianças com mais de 6 anos, pois pode ser bastante cansativo para os menores.",
    },
    {
      title: "Existe um código de vestimenta / dress code?",
      description:
        "Venham vestidos com o que se sentirem mais à vontade para uma comemoração de Ano-Novo. Queremos que todos se sintam bem e à vontade, assim como no Ton Hoi, com muito acolhimento.",
    },
    {
      title: "Há estacionamento disponível no local?",
      description:
        "O estacionamento é terceirizado pelo prédio e, infelizmente, não conseguimos parceria com eles desta vez. Portanto, o pagamento deve ser feito diretamente a eles. Nossos manobristas estarão na porta do estacionamento para ajudar a identificar o local da entrada do evento, mas não poderão manobrar os veículos. Recomendamos fortemente que venham de motorista de aplicativo para que possam aproveitar até o último minuto sem se preocupar com a direção após o evento. Segurança em primeiro lugar!",
    },
    {
      title: "Por que há diferença no preço dos ingressos?",
      description:
        "A primeira diferença está na forma de pagamento: as taxas para cartões são significativamente mais altas em comparação ao PIX. Em relação aos lotes, quanto maior a antecedência e disponibilidade, melhores os preços, similar ao que ocorre com passagens aéreas. Portanto, antecipe-se e garanta seu convite!",
    },
    {
      title: "Por que estou recebendo mensagens pelo whatsApp para a compra? ",
      description:
        "Para aumentar a segurança, além de inserir os primeiros dados de cadastro (nome, e-mail e CPF), enviamos um código de confirmação para garantir que não se trata de fraude ou BOT. Após a confirmação, você poderá escolher a forma de pagamento, e o link será enviado diretamente para o mesmo WhatsApp que recebeu o código.",
    },
    {
      title:
        "Não estou conseguindo clicar no link de pagamento. O que eu faço?",
      description:
        "Pedimos que salvem o contato no WhatsApp que recebeu as mensagens para que o link seja habilitado corretamente. Esta é uma funcionalidade definida pelo WhatsApp para evitar fraudes, então salvar o número é seguro. Após isso, o link funcionará corretamente.",
    },
    {
      title: "O pagamento está sendo direcionado para o Asaas. Está correto?",
      description:
        "Sim, está. Asaas é a plataforma parceira responsável pela cobrança.",
    },
    {
      title: "Como recebo a confirmação e/ou convite?",
      description:
        "Tanto a confirmação de compra quanto o convite com o QR code (que deverá ser apresentado no dia) chegam por link no mesmo contato de WhatsApp que receberam os códigos de confirmação e link de pagamento. Por isso, fiquem tranquilos, salvem o contato, é seguro!",
    },
    {
      title: "Posso comprar mais de um convite?",
      description:
        "Sim, claro! Você pode comprar quantos convites quiser. Primeiro, selecione a forma de pagamento e, em seguida, adicione a quantidade desejada de convites ao carrinho. A quantidade selecionada aparecerá no carrinho e você poderá finalizar a compra com o link que receberá no WhatsApp. Após isso, outro link será enviado para que você insira o nome de todos os convidados. Assim, no dia do evento, poderemos agilizar a confirmação da sua compra e você poderá aproveitar a festa o quanto antes.",
    },
    {
      title: "É possível sair e retornar à festa após a entrada?",
      description:
        "Infelizmente, não será permitido. Seu convite permite apenas uma única entrada. Após sair, o sistema dará baixa e será necessário adquirir outro convite (se ainda houver disponibilidade, o que é pouco provável devido à alta demanda)",
    },
    {
      title: "Posso comprar convites na entrada?",
      description:
        "Se houver convites remanescentes, sim, eles estarão disponíveis pelo valor de R$980,00. No entanto, devido à alta demanda antes mesmo do lançamento, é pouco provável que restem convites. Recomendamos que se antecipem e garantam seus convites com antecedência.",
    },
    {
      title: "O local possui área para fumantes?",
      description:
        "Sim, há uma área externa sem cobertura onde é permitido fumar. No entanto, em caso de chuva, não será permitido fumar sob a cobertura devido às normas de segurança.",
    },
  ];

  const { mobile } = useWindowDimensions();

  const handleClick = (_e, opt) => {
    setActiveIndex(opt.index);
  };

  const mountFaq = () => {
    return faqText.map((data, i) => {
      return (
        <div key={i + 1}>
          <AccordionTitle
            active={activeIndex === i}
            index={i}
            onClick={handleClick}
          >
            <Icon name="dropdown" />
            {data.title}
          </AccordionTitle>
          <AccordionContent active={activeIndex === i}>
            <p>{data.description}</p>
          </AccordionContent>
        </div>
      );
    });
  };

  return (
    <div className="frame">
      <Top />
      <div className="central" style={{ margin: 25 }}>
        <div
          className="pages"
          style={{ width: mobile ? "100%" : 600, marginBottom: 25 }}
        >
          <Container fluid>
            <Container textAlign="justified">
              <Header as="h2">Faq</Header>
              <Divider />
              <Accordion styled>{mountFaq()}</Accordion>
            </Container>
          </Container>
        </div>
      </div>
    </div>
  );
};
